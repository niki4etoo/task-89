import EventEmitter from "eventemitter3";
import image from "../images/planet.svg";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    
	this._loading = document.querySelector('progress');
	this._loading.style = "display:none";
	this._startLoading();
	this._load();    
	
    this.emit(Application.events.READY);
  }

  async _load(){
	  const planets = await fetch("https://swapi.boom.dev/api/planets")
		.then(result => result.json())
		.then(data => {
				
				var pageAvailable = true;
				var counter = 0;
				
				while(pageAvailable){
					counter++;
					
					fetch(`https://swapi.boom.dev/api/planets?page=${counter}`)
					.then(result => result.json())
					.then(pdata => {
							for(var planet of pdata.results){
								this._create(planet);
							}
							
						});
												
					if(counter === 6) pageAvailable = false;	
				}				
		})
		.then(() => { this._stopLoading(); });
  }
  
  _create(planet){
	  const box = document.createElement("div");
				box.classList.add("box");
				box.innerHTML = this._render({
					  name: planet.name,
					  terrain: planet.terrain,
					  population: planet.population,
					});
				document.body.querySelector(".main").appendChild(box);
  }
  
  _startLoading(){
	  this._loading.style = "display: inline";
  }
  
  _stopLoading(){
	  this._loading.style = "display: none";
  }


  _render({ name, terrain, population }) {
    return `
<article class="media">
  <div class="media-left">
    <figure class="image is-64x64">
      <img src="${image}" alt="planet">
    </figure>
  </div>
  <div class="media-content">
    <div class="content">
    <h4>${name}</h4>
      <p>
        <span class="tag">${terrain}</span> <span class="tag">${population}</span>
        <br>
      </p>
    </div>
  </div>
</article>
    `;
  }
}
